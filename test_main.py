#encoding: utf-8

from flask import Flask, views, jsonify, render_template, request, url_for
from functools import wraps
import config
from blueprints.user import user_bp
from blueprints.news import news_bp

app = Flask(__name__)
# 引入配置文件
app.config.from_object(config)

# 注册蓝图
app.register_blueprint(user_bp)
app.register_blueprint(news_bp)

########### 基本类视图 ###########
# 有几个url都需要返回json数据
class JSONView(views.View):
    def get_data(self):
        raise NotImplementedError

    # 类视图必须写的函数：dispatch_request()
    def dispatch_request(self):
        return jsonify(self.get_data())

class ListView(JSONView):
    def get_data(self):
        return {"username": 'test', "password": '123456'}

app.add_url_rule('/list/', endpoint='list', view_func=ListView.as_view('list'))

# 有几个页面都需要同样的变量
class ADSView(views.View):
    def __init__(self):
        super(ADSView, self).__init__()
        self.context = {
            'ads': u'今年过节不收礼'
        }

class LoginView(ADSView):
    def dispatch_request(self):
        self.context.update({'login_ads': u'登录专属广告'})
        return render_template('test/class_view_app/login.html', **self.context)

class RegistView(ADSView):
    def dispatch_request(self):
        return render_template('test/class_view_app/regist.html', **self.context)

app.add_url_rule('/login/', view_func=LoginView.as_view('login'))
app.add_url_rule('/regist/', view_func=RegistView.as_view('regist'))


########### 基于调度方法的类视图 ###########
class MLoginView(views.MethodView):
    def __render(self, error=None):
        return render_template('test/class_view_app/mlogin.html', error=error)

    def get(self):
        return self.__render()

    def post(self):
        username = request.form.get('username')
        password = request.form.get('password')
        if username == 'tim' and password == '111':
            return u'登录成功'
        else:
            return self.__render(error=u'用户名或密码错误')

app.add_url_rule('/mlogin/', view_func=MLoginView.as_view('mlogin'))


#########类视图中使用装饰器##########
def login_reuired(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        username = request.args.get('username')
        if username and username == 'tim':
            return func(*args, **kwargs)
        else:
            return u'请先登录'
    return wrapper

class Login(views.View):
    # 通过decorators=[]添加装饰器。可以添加多个，但注意前后顺序。
    decorators = [login_reuired]
    def dispatch_request(self):
        return u'登录后的页面'

app.add_url_rule('/c_login/',view_func=Login.as_view('clogin'))


@app.route('/blueprint/')
def blueprint():
    # url_for反转蓝图中的视图函数，需加上蓝图名称`blueprintName.viewFunName`
    return url_for('news.list')


@app.route('/')
def index():
    return 'hello world'

if __name__ == '__main__':
    # 模板内容修改后，自动重启服务器
    app.config['TEMPLATES_AUTO_RELOADE'] = True
    # 运行本项目，host=0.0.0.0可以让其他电脑也能访问到该网站，port指定访问的端口。默认的host是127.0.0.1，port为5000
    # app.run(host='0.0.0.0',port=9000)
    app.run(port=9000)
# -*- coding:utf-8 -*-
# 装饰器定义文件

from flask import redirect, url_for, session
from functools import wraps

# 定义登录限制的装饰器
def login_required(func):
    """
    要求必须登录。如果没登录，则跳转到登录页。
    :param func:
    :return:
    """
    @wraps(func)
    def wrapper(*args,**kwargs):
        if session.get('user_id'):
            return func(*args,**kwargs)
        else:
            return redirect(url_for('login'))

    return wrapper
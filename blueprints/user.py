# -*- coding:utf-8 -*-
from flask import Blueprint

user_bp = Blueprint('user', __name__, url_prefix='/user')

@user_bp.route('/personal/')
def personal():
    return u'个人中心页'

@user_bp.route('/settings/')
def settings():
    return u'设置页'

@user_bp.route('/index/')
def index():
    return u'个人模块首页'
# -*- coding:utf-8 -*-
from flask import Blueprint

news_bp = Blueprint('news', __name__, url_prefix='/news')

@news_bp.route('/list/')
def list():
    return u'新闻列表页'

@news_bp.route('/detail/')
def detail():
    return u'新闻详情页'

@news_bp.route('/index/')
def index():
    return u'新闻模块首页'
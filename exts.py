# -*- coding:utf-8 -*-
# 扩展定义文件

from flask_sqlalchemy import SQLAlchemy

# 初始化SQLAlchemy对象
db = SQLAlchemy()

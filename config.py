# -*- coding:utf-8 -*-
import os
from datetime import timedelta

# session相关
# SECRET_KEY 必须是24位字符串
SECRET_KEY = os.urandom(24)     # os.urandom()生成24位随机字符串。但是每次重启服务器后，key都会变化，这样所有session都会失效
# SECRET_KEY = 'j5f00UFJNI35dghw3dfHBhjf'     # 使用固定的key，可以避免服务器重启后session失效

# 设置session过期时间（假如启用，默认31天）
PERMANENT_SESSION_LIFETIME = timedelta(days=7)


# 数据库连接配置信息
# 格式 =>  dialect+driver://username:password@host:port/database
DIALECT = 'mysql'
DRIVER = 'mysqldb'
USERNAME = 'root'
PASSWORD = 'root'
HOST = '127.0.0.1'
PORT = '3306'
DATABASE = 'qa_demo'
DB_URL = "{}+{}://{}:{}@{}:{}/{}?charset=utf8".format(DIALECT,DRIVER,USERNAME,PASSWORD,HOST,PORT,DATABASE)

SQLALCHEMY_DATABASE_URI = DB_URL

SQLALCHEMY_TRACK_MODIFICATIONS = False

# 假如是生产环境，应该设为False
DEBUG = True
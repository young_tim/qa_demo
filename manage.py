# -*- coding:utf-8 -*-

from qa import app
from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand
from exts import db
from models import User, Question, Answer

manager = Manager(app)

# 使用Migrate绑定APP和db
migrate = Migrate(app,db)

# 添加数据库迁移脚本的命令到manager中
manager.add_command('db',MigrateCommand)

# 数据库模型初始化命令
# python manage.py db init
# 生成数据库模型迁移文件
# python manage.py db migrate
# 把模型映射到数据库中
# python manage.py db upgrade

if __name__ == '__main__':
    manager.run()